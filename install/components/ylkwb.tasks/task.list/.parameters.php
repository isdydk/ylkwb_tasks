<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentParameters = array(
    "GROUPS" => array(
        "SETTINGS" => array(
            "NAME" =>  Loc::getMessage('YLKWB_TASKS_PARAMS'),
            "SORT" => "500",
        ),
    ),
   "PARAMETERS" => array(
        "DATE_1" => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage('YLKWB_TASKS_PARAMS_DATE_1'),
            "TYPE" => "DATE",
        ),
        "DATE_1" => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage('YLKWB_TASKS_PARAMS_DATE_2'),
            "TYPE" => "DATE",
        ),
    )
);?>