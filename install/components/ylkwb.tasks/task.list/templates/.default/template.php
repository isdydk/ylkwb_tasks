<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if (isset($arResult['TASKS']) && is_array($arResult['TASKS']) && count($arResult['TASKS']) > 0):?>
    <? foreach ($arResult['TASKS'] as $task):?>
    <div>
        <h2><?=$task['TITLE']?></h2>
    </div>
    <?endforeach;?>    
<?endif;?>

