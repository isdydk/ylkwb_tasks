<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


class TaskListComponent extends \CBitrixComponent
{	
    public function executeComponent()
    {   
        if (\Bitrix\Main\Loader::includeModule('ylkwb.tasks'))
        {
            $date1 = isset($this->arParams['DATE_1']) && !empty($this->arParams['DATE_1'])
                    ? new DateTime($this->arParams['DATE_1']) : new DateTime();
            
            $date2 = isset($this->arParams['DATE_2']) && !empty($this->arParams['DATE_2'])
                    ? new DateTime($this->arParams['DATE_2']) : new DateTime();
            
            $controller = new \Ylkwb\Tasks\Controller($date1, $date2);
            
            $this->arResult['TASKS'] = $controller->getTasks();
        }
            
        $this->includeComponentTemplate();  
    }
}?>