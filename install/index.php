<?

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ylkwb_tasks extends CModule 
{
    public function __construct() 
    {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");

        $this->MODULE_ID = 'ylkwb.tasks';

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("YLKWB_TASKS_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("YLKWB_TASKS_MODULE_DESC");

        $this->PARTNER_NAME = Loc::getMessage("YLKWB_TASKS_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("YLKWB_TASKS_PARTNER_URI");

        $this->MODULE_SORT = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
        $this->MODULE_GROUP_RIGHTS = "Y";
    }

    //Определяем место размещения модуля
    public function GetPath() 
    {
        return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    public function InstallFiles()
    {
        $pathComponents = $this->GetPath() . "/install/components";
        $pathPageTasks = $this->GetPath() . "/install/page/tasks";

        if (\Bitrix\Main\IO\Directory::isDirectoryExists($pathComponents))
        {
            CopyDirFiles($pathComponents, $_SERVER["DOCUMENT_ROOT"] . "/local/components", true, true);
        }
        else
            throw new \Bitrix\Main\IO\InvalidPathException($pathComponents);
        
        if (\Bitrix\Main\IO\Directory::isDirectoryExists($pathPageTasks)) 
        {
            CopyDirFiles($pathPageTasks, $_SERVER["DOCUMENT_ROOT"] . '/tasks', true, true);
        }
        else
            throw new \Bitrix\Main\IO\InvalidPathException($pathPageTasks);

        return true;
    }

    public function UnInstallFiles()
    {
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . '/local/components/ylkwb.tasks/');
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . '/tasks/ylkwb.tasks/');

        return true;
    }

    public function InstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
            'rest',
            'OnRestServiceBuildDescription',
            'ylkwb.tasks',
            '\Ylkwb\Tasks\Service',
            'getDescription'
        );

        return true;
    }

    public function UnInstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
            'rest',
            'OnRestServiceBuildDescription',
            'ylkwb.tasks',
            '\Ylkwb\Tasks\Service',
            'getDescription'
        );

        return true;
    }

    public function DoInstall() 
    {
        global $APPLICATION;

        if ($this->isVersionD7()) {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallFiles();
            $this->InstallEvents();
        } else {
            $APPLICATION->ThrowException(Loc::getMessage("YLKWB_TASKS_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("YLKWB_TASKS_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallEvents();

        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(Loc::getMessage("YLKWB_TASKS_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep.php");
    }
}

?>