<?php

namespace Ylkwb\Tasks\Rest;

class TasksService
{
    // Возвращает описание доступных API методов
    public static function getDescription()
    {
        $methods = [];

        // Имя методов может быть произвольным но принято использовать следующий формат "имя_разрешения.имя_типа.действие"
        $methods['ylkwb.tasks.list'] = [];

        // Обработчик метода PHP псевдо-тип callback. Анонимные функции пока не поддерживаются.
        $methods['ylkwb.tasks.list']['callback'] = [static::class, 'list'];

        // Не совсем понятно что это и для чего используется
        // Смог найти только что если передать ключ "private" => true то вроде как метод будет считаться приватным
        $methods['ylkwb.tasks.list']['options'] = [];

        return $methods;

    }

    public static function list(array $query, int $start, \CRestServer $server)
    {
        $arResult = [];
        
        if (\Bitrix\Main\Loader::includeModule('ylkwb.tasks'))
        {
            $date1 = isset($query['DATE_1']) && !empty($query['DATE_1'])
                    ? new \DateTime($query['DATE_1']) : new \DateTime();
            
            $date2 = isset($query['DATE_2']) && !empty($query['DATE_2'])
                    ? new \DateTime($query['DATE_2']) : new \DateTime();
            
            $controller = new \Ylkwb\Tasks\Controller($date1, $date2);
            
            $arResult = $controller->getTasks();
        }
        
        return $arResult;
    }

}