<?php

namespace Ylkwb\Tasks;

use Ylkwb\Tasks\Rest\TasksService;

class Service
{
    // Возвращает описание одного или нескольких разрешений
    public static function getDescription()
    {
        $scopes = [];

        $scopes[\CRestUtil::GLOBAL_SCOPE] = [];

        $scopes[\CRestUtil::GLOBAL_SCOPE] = array_merge(
            $scopes[\CRestUtil::GLOBAL_SCOPE],
            TasksService::getDescription()
        );

        return $scopes;
    }
}
