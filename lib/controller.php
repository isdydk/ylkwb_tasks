<?php

namespace Ylkwb\Tasks;

use \Ylkwb\Tasks\Model;

class Controller
{
    protected $filterDate1;
    protected $filterDate2;
    
    public function __construct(\DateTime $filterDate1, \DateTime $filterDate2)
    {
        $this->filterDate1 = $filterDate1;  
        $this->filterDate2 = $filterDate2;  
    }
    
    public function getTasks() : array
    {
        $order = [
            "ID" => "ASC"
        ];
        
        $select = [
            'ID'
        ];
                
        //Д1<=дата создания<=Д2;
        $result1 = Model::getTasks(
            $order,
            [
                ">=CREATED_DATE" => $this->filterDate1->format(Model::TASKS_FORMAT_DATE),
                "<=CREATED_DATE" => $this->filterDate2->format(Model::TASKS_FORMAT_DATE),
            ], 
            $select
        );
             
        //Д1<=дата изменения<=Д2;
        $result2 = Model::getTasks(
            $order,
            [
                ">=CHANGED_DATE" => $this->filterDate1->format(Model::TASKS_FORMAT_DATE),
                "<=CHANGED_DATE" => $this->filterDate2->format(Model::TASKS_FORMAT_DATE),
            ], 
            $select
        );
             
        //Д1<=дата закрытия<=Д2;
        $result3 = Model::getTasks(
            $order,
            [
                ">=CLOSED_DATE" => $this->filterDate1->format(Model::TASKS_FORMAT_DATE),
                "<=CLOSED_DATE" => $this->filterDate2->format(Model::TASKS_FORMAT_DATE),
            ], 
            $select
        );
             
        //дата создания<=Д1 И статус не закрыт
        $result4 = Model::getTasks(
            $order,
            [
                "<=CREATED_DATE" => $this->filterDate1->format(Model::TASKS_FORMAT_DATE),
                "!REAL_STATUS" => [
                    \CTasks::STATE_SUPPOSEDLY_COMPLETED,
                    \CTasks::STATE_COMPLETED,
                    \CTasks::STATE_DEFERRED,
                ],
            ], 
            $select
        );
             
        //дата создания<=Д1 И дата закрытия>=Д2
        $result5 = Model::getTasks(
            $order,
            [
                "<=CREATED_DATE" => $this->filterDate1->format(Model::TASKS_FORMAT_DATE),
                ">=CLOSED_DATE" => $this->filterDate2->format(Model::TASKS_FORMAT_DATE),
            ], 
            $select
        );
        
        //Результат
        $result = array_unique(
            array_merge(
                $this->getItemsValue('ID', $result1), 
                $this->getItemsValue('ID', $result2), 
                $this->getItemsValue('ID', $result3), 
                $this->getItemsValue('ID', $result4), 
                $this->getItemsValue('ID', $result5), 
            )
        );
        
        if (is_array($result) && count($result) > 0)
        {
            $arResult = Model::getTasks(
                [
                    'ID' => 'DESC'
                ],
                [
                    "=ID" => $result,
                ], 
                [
                    'ID', 'TITLE'
                ]
            );
            
            return $arResult;
        }
        
        return [];
    }
    
    protected function getItemsValue($key, $array) {
        return array_values(array_map(function ($item) use ($key) { 
            return $item[$key];
        }, $array));
    }
    
}