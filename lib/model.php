<?php

namespace Ylkwb\Tasks;

class Model
{
    const TASKS_FORMAT_DATE = 'd.m.Y';
    
    public static function getTasks(array $arOrder, array $arFilter, array $arSelect) : array
    {
        $arItems = [];
                
        if (\Bitrix\Main\Loader::includeModule("tasks"))
        {
            if (!in_array('ID', $arSelect))
            {
                $arSelect[] = 'ID';
            }
            
            $res = \CTasks::GetList(
                $arOrder,
                $arFilter, 
                $arSelect
            );

            while ($arItem = $res->GetNext())
            {
                $arItems[$arItem['ID']] = $arItem;
            }
        }

        return $arItems;
    }
}